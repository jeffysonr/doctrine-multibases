
--
-- Database: `db_banco_um`
--

-- --------------------------------------------------------

CREATE DATABASE `db_banco_um`;

--
-- Table structure for table `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(250) NOT NULL,
  `quantidade` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `descricao`, `quantidade`) VALUES
(1, 'produto 01', 'produto 01', 10),
(2, 'produto 02', 'produto 02', 12),
(3, 'produto 03', 'produto 03', 15),
(4, 'produto 04', 'produto 04', 20),
(5, 'produto 05', 'produto 05', 13),
(6, 'produto 06', 'produto 06', 18);


--
-- Database: `db_banco_dois`
--

-- --------------------------------------------------------

CREATE DATABASE `db_banco_dois`;

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `qtde` int(10) NOT NULL,
  `referencia` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `qtde`, `referencia`) VALUES
(1, 'Prod 01', 2, 6),
(2, 'Prod 02', 2, 4),
(3, 'Prod 03', 2, 1),
(4, 'Prod 04', 2, 5);

