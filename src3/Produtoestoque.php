<?php

/**
 * Produtoestoque
 *
 * @Entity
 * @Table(name="ps_brdistribuidorstock_available")
 */
class Produtoestoque
{
    const MARGEM_DE_ERRO = 50;
    /**
     * @Id
     * @Column(type="integer", name="id_product")
     */
    protected $id;
    
    /**
     * @Column(type="string", name="quantity")
     */
    protected $quantidade;


    public function getId() {
        return $this->id;
    }
 
    public function getQuantidade() {
        return $this->quantidade;
    }
 
    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade - self::MARGEM_DE_ERRO;
    }
}

?>