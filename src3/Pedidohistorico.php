<?php

/**
 * Pedidohistorico
 *
 * @Entity
 * @Table(name="ps_brdistribuidororder_history")
 */
class Pedidohistorico
{
    /**
     * @Id
     * @Column(type="integer", name="id_order_history")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Column(type="integer", name="id_order")
     */
    protected $pedido;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $verificado;


    public function getId() {
        return $this->id;
    }
 
    public function getPedido() {
        return $this->pedido;
    }
 
    public function getVerificado() {
        return $this->verificado;
    }
 
    public function setVerificado($verificado) {
        $this->verificado = ($verificado) ? '1' : '0';
    }
}

?>