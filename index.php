<?php 
ini_set('display_errors','On');
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once 'bootstrap.php';
require_once 'src1/Produto.php';
require_once 'src2/Product.php';

$produtos = $entityManager1->getRepository('Produto')->findAll();

$contUpdateProdutos = 0;

foreach ($produtos as $produto) {
	$product = $entityManager2->getRepository('Product')->findOneBy(array('referencia' => $produto->getId()));

	if (is_object($product)) {
		if ($produto->getQuantidade() != $product->getQtde()) {

			$product->setQtde($produto->getQuantidade());
			$entityManager2->persist($product);
			$entityManager2->flush();

			$contUpdateProdutos++;
		}
	}
}

echo "{$contUpdateProdutos} Produtos foram atualizados em: " . date('d/m/Y H:i:s');

?>