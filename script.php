<?php 
ini_set('display_errors','On');
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once 'bootstrap.php';
require_once 'src1/Produto.php';
require_once 'src3/Pedidohistorico.php';
require_once 'src3/Produtoestoque.php';

//entityManager1 - CONEXAO COM BANCO DE DADOS DO CONTROLE DE ESTOQUE
//entityManager3 - CONEXAO COM BANCO DE DADOS DO PESTASHOP

//INICIANDO TRANSAÇÃO COM BANCO DO PESTASHOP
$entityManager3->getConnection()->beginTransaction();

try {
	$sql = "
		SELECT od.product_id, od.product_quantity, LOWER(TRIM(p.reference)) AS reference, oh.id_order_history, oh.verificado
	        FROM ps_brdistribuidororder_detail od
	        INNER JOIN ps_brdistribuidorproduct p ON p.id_product = od.product_id
	        INNER JOIN ps_brdistribuidororder_history oh ON oh.id_order = od.id_order AND oh.verificado IS FALSE
	        INNER JOIN ps_brdistribuidororder_state os ON os.id_order_state = oh.id_order_state AND os.paid IS TRUE
		GROUP BY od.product_id
	";

	//LISTANDO TODOS OS PEDIDOS PAGOS QUE AINDA NÃO FORAM VERIFICADOS (NAO COMPUTADOS NO ESTOQUE)
	$query = $entityManager3->getConnection()->prepare( $sql );
	$query->execute();

	$pedidos = $query->fetchAll();

	//INICIANDO TRANSAÇÃO COM BANCO DO CONTROLE DE ESTOQUE
	$entityManager1->getConnection()->beginTransaction();

	try {
		foreach ($pedidos as $k => $pedido) {
			$sqlUpdate = "UPDATE produtos SET quantidade = quantidade - ".$pedido['product_quantity']." 
				WHERE LOWER(TRIM(referencia)) = '".$pedido['reference']."'";

			//DEBITANDO QUANTIDADES DOS PRODUTOS COMPRADOS NO SITE DO ESTOQUE ORIGINAL
			$qUpdate = $entityManager1->getConnection()->prepare( $sqlUpdate );
			$qUpdate->execute();

			//MARCANDO PEDIDO DO SITE COMO VERIFICADO
			$pedidoHistorico = $entityManager3->find('Pedidohistorico', $pedido['id_order_history']);
			$pedidoHistorico->setVerificado(true);
			$entityManager3->persist($pedidoHistorico);
			$entityManager3->flush();


			$sql2 = "SELECT quantidade, referencia FROM produtos 
					WHERE LOWER(TRIM(referencia)) = '".$pedido['reference']."' LIMIT 1";

			//PEGANDO A QUANTIDADE DE PRODUTOS DO ESTOQUE ORIGINAL APOS ATUALIZAÇÃO
			$query2 = $entityManager1->getConnection()->prepare( $sql2 );
			$query2->execute();
			$produto = $query2->fetch();

			//ATUALIZANDO ESTOQUE DO SITE DE ACORDO COM O ESTOQUE ORIGINAL
			$produtoEstoque = $entityManager3->find('Produtoestoque', $pedido['product_id']);
			$produtoEstoque->setQuantidade($produto['quantidade']);
			$entityManager3->persist($produtoEstoque);
			$entityManager3->flush();

		}

		//CONFIRMANDO ALTERAÇÕES DOS DADOS NOS BANCOS
		$entityManager1->getConnection()->commit();
		$entityManager3->getConnection()->commit();

	} catch (Exception $e) {
		//REINICIANDO ALTERAÇÕES CASO HAJA ALGUM ERRO NO BANCO DO ESTOQUE
		$entityManager1->getConnection()->rollback();
		echo $e->getMessage();
	}

} catch (Exception $e) {
	//REINICIANDO ALTERAÇÕES CASO HAJA ALGUM ERRO NO BANCO DO PRESTASHOP
	$entityManager3->getConnection()->rollback();
	echo $e->getMessage();
}

