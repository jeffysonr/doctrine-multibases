<?php

// bootstrap.php
 
//vamos configurar a chamada ao Entity Manager, o mais importante do Doctrine
 
// o Autoload é responsável por carregar as classes sem necessidade de incluí-las previamente
require_once "vendor/autoload.php";
 
// o Doctrine utiliza namespaces em sua estrutura, por isto estes uses
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
 
$isDevMode = true;

//CONFIGURAÇÃO BANCO 1

//onde irão ficar as entidades do projeto? Defina o caminho aqui
$entidades1 = array("src1/");
 
// configurações de conexão. Coloque aqui os seus dados
$dbParams1 = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => '123',
    'dbname'   => 'db_banco_um',
    //'unix_socket' => '/Applications/MAMP/tmp/mysql/mysql.sock'
);

//setando as configurações definidas anteriormente
$config1 = Setup::createAnnotationMetadataConfiguration($entidades1, $isDevMode);

//criando o Entity Manager com base nas configurações de dev e banco de dados
$entityManager1 = EntityManager::create($dbParams1, $config1);



//CONFIGURAÇÃO BANCO 2

$entidades2 = array("src2/");

$dbParams2 = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => '123',
    'dbname'   => 'db_banco_dois',
    //'unix_socket' => '/Applications/MAMP/tmp/mysql/mysql.sock'
);

//setando as configurações definidas anteriormente
$config2 = Setup::createAnnotationMetadataConfiguration($entidades2, $isDevMode);

//criando o Entity Manager com base nas configurações de dev e banco de dados
$entityManager2 = EntityManager::create($dbParams2, $config2);



//CONFIGURAÇÃO BANCO 3

$entidades3 = array("src3/");

$dbParams3 = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => '123',
    'dbname'   => 'bufalus_1',
    //'unix_socket' => '/Applications/MAMP/tmp/mysql/mysql.sock'
);

//setando as configurações definidas anteriormente
$config3 = Setup::createAnnotationMetadataConfiguration($entidades3, $isDevMode);

//criando o Entity Manager com base nas configurações de dev e banco de dados
$entityManager3 = EntityManager::create($dbParams3, $config3);


?>