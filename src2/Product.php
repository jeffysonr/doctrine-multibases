<?php

/**
 * Product
 *
 * @Entity
 * @Table(name="products")
 */
class Product
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="string")
     */
    protected $name;

    /**
     * @Column(type="integer")
     */
    protected $qtde;

    /**
     * @Column(type="integer")
     */
    protected $referencia;
 

    public function getId() {
        return $this->id;
    }
 
    public function getName() {
        return $this->name;
    }
 
    public function getQtde() {
        return $this->qtde;
    }

    public function getReferencia() {
        return $this->referencia;
    }
 
    public function setName($name) {
        $this->name = $name;
    }

    public function setQtde($qtde) {
        $this->qtde = $qtde;
    }

    public function setReferencia($qtde) {
        $this->referencia = $referencia;
    }
    
}

?>