<?php

/**
 * Produto
 *
 * @Entity
 * @Table(name="produtos")
 */
class Produto
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="string")
     */
    protected $nome;

    /**
     * @Column(type="string")
     */
    protected $descricao;

    /**
     * @Column(type="integer")
     */
    protected $quantidade;
 

    public function getId() {
        return $this->id;
    }
 
    public function getNome() {
        return $this->nome;
    }

    public function getDescricao() {
        return $this->descricao;
    }
 
    public function getQuantidade() {
        return $this->quantidade;
    }
 
    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }
    
}

?>